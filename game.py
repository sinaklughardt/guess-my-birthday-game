from random import randint

name = input("Hi! What is your name? ")
for guess_number in range(5):
    month_number = randint(1, 12)
    year_number = randint(1924, 2005)

    print("Guess: ", guess_number + 1, ":" ,
    name, "were you born on", month_number, "/", year_number, "?")
    response = input("yes or no? ")

    if response == "yes":
        print("I knew it!")
        break

    elif response == "no":
        if guess_number == 4:
            break
        print("Drat! Lemme try again!")

if guess_number == 4:
    print("I have other things to do. Good bye")
